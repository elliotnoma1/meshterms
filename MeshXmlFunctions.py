##############   extract information from the descriptor block
import xmltodict, json

def convertXmlToJson(block):
    xblock = [str(x).replace("b'","").replace('\\n','').replace("\'","") for x in block]
    xblockString = ''.join(xblock)
    o = xmltodict.parse(xblockString)    #jsonBlock = json.dumps(o) # '{"e": {"a": ["text", "text"]}}'
    return o

def getDescriptor(block):
    o = convertXmlToJson(block)
    descriptorClass = o['DescriptorRecord']['@DescriptorClass']
    UI = o['DescriptorRecord']['DescriptorUI']
    name = o['DescriptorRecord']['DescriptorName']['String']
    try:
        treeNumber = o['DescriptorRecord']['TreeNumberList']['TreeNumber']
    except:
        treeNumber = []  # some terms, such as 'Female' do not have a TreeNumber 
    return (descriptorClass, UI, name, treeNumber)

def getClasses(d):   #  descriptor[3] = list of MeSH term classes for this descriptor, return a letter from A to Z
    if type(d) is str:
        return [d[0]]
    else:
        return list(set([x[0] for x in d]))
    
#getClasses(getDescriptor(block)[3])

################# get terms for a target class

def getTermsForTargetClass(targetClass, starts, mesh):
    terms = []
    for blockN in list(range(len(starts)))[:-1]:
        block = mesh[starts[blockN]: starts[blockN + 1]]
        descriptor = getDescriptor(block)
        if targetClass in getClasses(descriptor[3]):
            terms.append((blockN,descriptor[2]))
    return terms

############  get concepts from a target block

def getConcepts(block):
    o = convertXmlToJson(block)
    try:
        conceptList = [x['ConceptName']['String'] for x in o['DescriptorRecord']['ConceptList']['Concept']]
    except:
        conceptList = []
    try:
        termList = [[y['String'] for y in x['TermList']['Term']] for x in o['DescriptorRecord']['ConceptList']['Concept']]
    except:
        termList = []
    concepts = conceptList.copy()
    [concepts.extend(x) for x in termList]
    return sorted(list(set(concepts)))

'''termN = [x for x in CtermList if x[1] == 'Neoplasms'][0]  # [(10478, 'Neoplasms')]
block = mesh[starts[termN[0]]: starts[termN[0] + 1]]
[termN, getConcepts(block)]
'''
def getTermListWithConcepts(termList, starts, mesh):
    conceptTermList = []
    for termN in termList:
        block = mesh[starts[termN[0]]: starts[termN[0] + 1]]
        conceptTermList.append([termN, getConcepts(block)])
    return conceptTermList

######### get the start of each descriptor block in xml

def getStartingPointersToDesciptors(mesh):
    descriptorSetDivider = '</DescriptorRecordSet>'  # only one block in file
    descriptorDivider = '<DescriptorRecord '   # 28939 descriptor blocks in Mesh file

    descriptorBlockStart = [i for i,m in enumerate(mesh) if str(m).find(descriptorDivider) > 0]
    print('{} descriptor blocks in Mesh file'.format(len(descriptorBlockStart)))
    descriptorSetBlockEnd = [i for i,m in enumerate(mesh) if str(m).find(descriptorSetDivider) > 0][0]
    print('location of terminating tag: {}'.format(descriptorSetBlockEnd))

    ################ pointers to the start of each descriptor block within xml

    starts = descriptorBlockStart.copy()
    starts.append(descriptorSetBlockEnd)
    return starts

################ count number of terms per class
from collections import Counter

def countTermsPerClass(starts,mesh):
    classes = []
    for blockN in list(range(len(starts)))[:-1]:
        block = mesh[starts[blockN]: starts[blockN + 1]]
        descriptor = getDescriptor(block)
        classes.extend(getClasses(getDescriptor(block)[3]))

    return Counter(classes)