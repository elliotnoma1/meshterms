###########  read the MeSh xml file and create word file
from MeshXmlFunctions import *

def createMeshTermFiles(inZipFile='desc2018.zip', inXmlFile='desc2018.xml'):
    import zipfile, pprint, pickle

    with zipfile.ZipFile(inZipFile) as myzip:
        with myzip.open(inXmlFile) as f:
            mesh = f.readlines()

    print('{} lines read in the MeSH terms file'.format(len(mesh)))

    starts = getStartingPointersToDesciptors(mesh)  # get pointers to start of each term in the xml
    print('number of terms per class')
    print(countTermsPerClass(starts,mesh))

    ###############  get terms in groups C and E and then get all concepts associated with those terms

    CtermList = getTermsForTargetClass('C', starts, mesh)
    EtermList = getTermsForTargetClass('E', starts, mesh)

    termConcepts = getTermListWithConcepts(CtermList, starts, mesh)
    termConcepts.extend(getTermListWithConcepts(EtermList, starts, mesh))

    allTerms = []   # flatten the list into a single list
    for c in termConcepts:
        allTerms.append(c[0][1])
        allTerms.extend(c[1])

    pickle.dump(dict([('Cterms',CtermList), ('Eterms',EtermList), ('allTermsAndConcepts',allTerms)]), open('MeshCandE.pickle','wb'))

    print('{} terms. {} words in terms + concept words'.format(len(CtermList)+len(EtermList), len(allTerms)))
    if __debug__:
        testProbe = 'Cancer'
        if testProbe not in allTerms: print('error: {} not in the list'.format(testProbe))

if __name__ == '__main__':
    createMeshTermFiles()